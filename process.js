const express = require("express");
const cors = require("cors");
const app = express();
var mysql = require("mysql");

/*-------------------add details of sqlConfig-----------------*/

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "biogassyst",
});
function addNewUser(userName, userEmail, userPassword) {
  con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
    var sql =
      "INSERT INTO users (name, email, password) VALUES ('" +
      userName +
      "', '" +
      userEmail +
      "', '" +
      userPassword +
      "')";
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("1 record inserted");
    });
  });
}

//use cors to allow cross origin resource sharing
app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
let users = [];
//POST
app.post("/create", function (req, res) {
  const newUser = {
    userName: req.body.userName,
    userEmail: req.body.userEmail,
    userPassword: req.body.userPassword,
  };
  users.push(newUser);
  console.log(users);
  addNewUser(newUser.userName, newUser.userEmail, newUser.userPassword);
});
//GET
app.get("/home", function (req, res) {
  console.log("Inside Home Login");
  res.writeHead(200, {
    "Content-Type": "application/json",
  });
  console.log("Users : ", JSON.stringify(users));
  res.end(JSON.stringify(users));
});
//start your server on port 3001
app.listen(3001, () => {
  console.log("Server Listening on port 3001");
});
